# Sistema de Gestión Ferreteria Juanito ASP.NET MVC y API

Este proyecto consiste en una aplicación ASP.NET MVC y una API desarrollada en C# utilizando Visual Studio. 

## Requisitos Previos

Antes de comenzar, asegúrate de tener los siguientes requisitos:

- [Visual Studio](https://visualstudio.microsoft.com/) (2022)
- .NET Framework (4.7)
- [SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads) (o cualquier base de datos que estés usando)
- [Serilog](https://serilog.net/) 


Sigue estos pasos para configurar el proyecto localmente.

### Clonar el Repositorio

```bash
git clone https://github.com/tu-usuario/tu-repositorio.git
cd tu-repositorio

CONFIGURACIÓN APLICACIÓN
Abrir el Proyecto en Visual Studio
- Selecciona Open a project or solution.
- Navega a la carpeta del proyecto y abre el archivo .sln.
- Abrir el archivo WebConfig y actualizar la ruta local de tu iis expres en la siguiente Key manteniendo la ruta /api/

 <add key="UrlApiGestion" value="http://localhost:51568/api/" />


BASE DE DATOS
-Abrir Sql Server
-Importar la Base de datos que se encuentra en el repositorio con el nombre BDGestionFJ

CONFIGURACIÓN API
Abrir el Proyecto en Visual Studio
- Selecciona Open a project or solution.
- Navega a la carpeta del proyecto y abre el archivo .sln.

Configurar la Base de Datos
Abre el archivo Web.config en el proyecto ASP.NET MVC.
Encuentra la cadena de conexión bajo <connectionStrings> y actualiza con los detalles de tu servidor SQL:
xml
Copiar código
<connectionStrings>
    <add name="DefaultConnection" connectionString="Server=TU_SERVIDOR;Database=TU_BASE_DE_DATOS;User Id=TU_USUARIO;Password=TU_CONTRASEÑA;" providerName="System.Data.SqlClient" />
</connectionStrings>


EJECUCIÓN
-Ejecutar la Aplicación ASP.NET MVC
-En Visual Studio, selecciona la solución del proyecto ASP.NET MVC.
-Haz clic en Start (o presiona F5) para ejecutar el proyecto.
-La aplicación debería iniciarse y abrirse en tu navegador predeterminado.

Ejecutar la API
-Selecciona la solución del proyecto de la API.
-Haz clic en Start (o presiona F5) para ejecutar el proyecto.
-La API debería iniciarse y estar disponible en la URL configurada.

PRUEBAS DE LA API
-Para realizar pruebas de los metodos de la api, debes ingresar a Postman e importar la carpeta new collection 
ubicada en el repositorio


